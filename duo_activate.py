#!/usr/bin/env python3

from pathlib import Path
import requests
import base64
import json
import sys
import cv2
from Crypto.PublicKey import RSA

def readQR(filepath):
  img = cv2.imread(qrFile.name)
  detector = cv2.QRCodeDetector()
  data, bbox, straight_qrcode = detector.detectAndDecode(img)
  if bbox is not None and data != "":
    print(f"QRCode data:\n{data}")
  # if no QR code was detected by cv2 let the user input the QR value
  else:
    print("No QR code detected in file.")
    data = input("Please enter QR Code value manually: ")
  return data


qrFile = Path("qr.png")

# if file exists read QR code, else user enters QR value manually
if qrFile.is_file():
  data = readQR(qrFile.as_posix())
else:
  print(qrFile.as_posix()+" not found.")
  data = input("Please enter QR Code value manually: ")


# The QR Code is in the format: XXXXXXXXXX-YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY
# where 'XXXXXXXXXX' is the "code"
code = data.split("-")[0]
# and 'YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY' is the host
# The host should be in the following format: api-XXXXX.duosecurity.com
b64host = data.split("-")[1]

# add missing base64 padding with ('=') for guaranteed successful base64 decode
# Source: https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
b64hostpadding = b64host + '=' * (-len(b64host) % 4)
# decode base64 and convert to string
host = base64.b64decode(b64hostpadding).decode('utf-8')

# Debugging
print("Code: "+code)
print("Host: "+host)

# Make the request to Duo's servers
url = 'https://{host}/push/v2/activation/{code}?customer_protocol=1'.format(host=host, code=code)
headers = {'User-Agent': 'okhttp/2.7.5'}
data = {'pkpush': 'rsa-sha512',
        'pubkey': RSA.generate(2048).public_key().export_key("PEM").decode(),
        'jailbroken': 'false',
        'architecture': 'arm64',
        'region': 'US',
        'app_id': 'com.duosecurity.duomobile',
        'full_disk_encryption': 'true',
        'passcode_status': 'true',
        'platform': 'Android',
        'app_version': '4.33.0',
        'app_build_number': '433000',
        'version': '13',
        'manufacturer': 'Google',
        'language': 'en',
        'model': 'Cheetah',
        'security_patch_level': '2024-01-01'}
r = requests.post(url, headers=headers, data=data)
response = json.loads(r.text)

# Check to see if the returned response gives us an hotp_secret.
try:
  secret = base64.b32encode(response['response']['hotp_secret'].encode())
except KeyError:
  print(response)
  sys.exit(1)
print("secret", secret)

# Output files duotoken.htop and response.json for export and generation
with open('duotoken.hotp', 'w') as f:
    f.write(secret.decode() + "\n")

with open('response.json', 'w') as resp:
    resp.write(r.text)
