#!/usr/bin/env python3

import pyqrcode
import json
import base64
import sys

# Make sure response.json exists.
try:
  with open("response.json", "r") as f:
    response = json.loads(f.read())['response']
except FileNotFoundError:
  print("response.json does not exist.")
  print("Please run duo_activate.py successfully first, prior to attempting to export the hotp secret.")
  sys.exit(1)

label = response['customer_name']
# base32 encoded hotp secret, with the padding ("=") stripped.
secret = base64.b32encode(bytes(response['hotp_secret'], 'utf-8')).decode('utf-8').replace('=', '')

# Create the QR Code and output to terminal
qrdata = 'otpauth://totp/{label}?secret={secret}&issuer=Duo&digits=6&period=30'.format(label=label, secret=secret)
qrcode = pyqrcode.create(qrdata)
print(qrcode.terminal(quiet_zone=1))
print(qrdata)
