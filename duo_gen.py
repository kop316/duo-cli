#!/usr/bin/env python3

import pyotp
import sys

if len(sys.argv) == 2:
    file = sys.argv[1]
else:
    file = "duotoken.hotp"

# Update duotoken.hotp
try:
    with open(file, "r+") as f:
        secret = f.readline()[0:-1]
        offset = f.tell()
        f.seek(offset)
except:
    print("duotoken.hotp does not exist.")
    print("Please run duo_activate.py successfully first, prior to attempting to generate TOTP.")
    sys.exit(1)


totp = pyotp.TOTP(secret)
print("Code:", totp.now())